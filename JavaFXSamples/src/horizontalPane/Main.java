package horizontalPane;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Main extends Application{
	@Override
	public void start(Stage primaryStage){
		try {
			//Set root and load resources from FXML
			AnchorPane root = (AnchorPane)FXMLLoader.load(getClass().getResource("FXML/MainScene.fxml"));
			//Set scene and add root to scene
			Scene scene = new Scene(root, 600, 400);
			//Show stage and scene
			primaryStage.setScene(scene);
			primaryStage.show();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}
