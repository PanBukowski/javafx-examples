package horizontalPane;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;

public class MainController {
	@FXML
	private AnchorPane leftPane;
	@FXML
	private Button leftPaneButton;
	//Add showPopUp action named just like in FXML file
	@FXML
	private void showLeftPane(ActionEvent event){
		try {
			AnchorPane table = new AnchorPane();
			//Load resources to new table
			table = (AnchorPane)FXMLLoader.load(getClass().getResource("FXML/PopUp.fxml"));
			//Add table to Parent Node
			leftPane.getChildren().add(table);
		} catch (IOException e) {
			e.printStackTrace();
		}
		leftPaneButton.setText("Hide");
		leftPaneButton.setOnAction(this::hideLeftPane);
	};
	@FXML
	private void hideLeftPane(ActionEvent event){
		leftPaneButton.setText("Show");
		leftPaneButton.setOnAction(this::showLeftPane);
		leftPane.getChildren().clear();
	}
	
}
