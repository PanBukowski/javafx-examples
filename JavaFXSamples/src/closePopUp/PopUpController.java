package closePopUp;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class PopUpController {
	//Define Button named like in FXML file
	@FXML
	private Button closeButton;
	//Define action closePopUp named like in FXML file
	@FXML
	private void closePopUp(ActionEvent event){
		//Get Stage window where button is
		Stage stage = (Stage) closeButton.getScene().getWindow();
		//Close that stage
		stage.close();
	}
	//Define action closeProgram named like in FXML file
	@FXML
	private void closeProgram(ActionEvent event){
		//Close entire Application
		Platform.exit();
	}
}
