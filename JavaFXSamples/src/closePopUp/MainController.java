package closePopUp;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class MainController {
	//Add showPopUp action named just like in FXML file
	@FXML
	private void showPopUp(ActionEvent event){
		try {
			//Define new Stage
			Stage popUpStage = new Stage();
			//Set new PopUp Windows always on top of application (windows below can't be edited)
			popUpStage.initModality(Modality.APPLICATION_MODAL);
			//Don't show Title Bar in PopUp window
			popUpStage.initStyle(StageStyle.UNDECORATED);
			//Set root and load resources from FXML
			AnchorPane root = (AnchorPane)FXMLLoader.load(getClass().getResource("FXML/PopUp.fxml"));
			//Set scene and add root to scene
			Scene scene = new Scene(root);
			//Show stage and scene
			popUpStage.setScene(scene);
			popUpStage.show();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}
